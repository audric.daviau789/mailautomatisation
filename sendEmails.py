import os
import openpyxl
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import re

# Informations de connexion SMTP pour Gmail
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587
SENDER_EMAIL = 'audric.daviau789@gmail.com'
EMAIL_PASSWORD = ''

def load_data_from_excel(excel_file_path):
    wb = openpyxl.load_workbook(excel_file_path, read_only=True)
    ws = wb.active

    data = {}
    for row in ws.iter_rows(min_row=2, values_only=True):
        nom, prenom, _, email = row

        # Nettoyage et normalisation du nom
        nom = nom.strip()  # Supprime les espaces au début et à la fin
        nom = nom.replace('-', ' ')  # Remplace les tirets par des espaces
        nom = nom.title()  # Met la première lettre de chaque mot en majuscule

        # Nettoyage et normalisation du prénom
        prenom = prenom.strip()  # Supprime les espaces au début et à la fin
        prenom = prenom.replace('-', ' ')  # Remplace les tirets par des espaces
        prenom = prenom.title()  # Met la première lettre de chaque mot en majuscule

        data[(nom, prenom)] = {'email': email}

    return data

def extract_nom_prenom_from_filename(filename, data):
    # Retire l'extension du nom du fichier
    filename = filename.split('.')[0]

    # Vérifie chaque adhérent pour voir s'il est dans le nom du fichier
    for nom, prenom in data:
        nom_lower = nom.lower()
        prenom_lower = prenom.lower()

        # Si le prénom est dans le nom du fichier, recherche le nom et le prénom
        if prenom_lower in filename.lower():
            pattern = f"{prenom_lower}.*{nom_lower}|{nom_lower}.*{prenom_lower}"
            if re.search(pattern, filename.lower()):
                return (nom, prenom)
        # Si le prénom n'est pas dans le nom du fichier, recherche seulement le nom
        else:
            if nom_lower in filename.lower():
                return (nom, prenom)

    # Si aucun adhérent n'a été trouvé, retourne None
    return None

def send_email_with_attachment(nom, prenom, recipient_email, attachment_path):
    # Créer le message e-mail
    subject = f"Billet de match pour {nom} {prenom}"
    body = f"Bonjour {nom} {prenom},\nVeuillez trouver ci-joint la place du match.\nCordialement,"
    
    # Fonction de l'envoi de l'e-mail, telle que précédemment générée
    msg = MIMEMultipart()
    msg['From'] = SENDER_EMAIL
    msg['To'] = recipient_email
    msg['Subject'] = subject

    msg.attach(MIMEText(body))

    with open(attachment_path, 'rb') as attachment:
        pdf_attachment = MIMEApplication(attachment.read(), _subtype='pdf')
        pdf_attachment.add_header('Content-Disposition', 'attachment', filename=attachment_path)
        msg.attach(pdf_attachment)

    try:
        smtp_conn = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        smtp_conn.starttls()
        smtp_conn.login(SENDER_EMAIL, EMAIL_PASSWORD)
        smtp_conn.send_message(msg)
        smtp_conn.quit()
        print(f"L'e-mail a été envoyé à {recipient_email}")
    except Exception as e:
        print(f"Une erreur s'est produite lors de l'envoi de l'e-mail à {recipient_email}: {e}")

def main():
    # Chemin du fichier Excel contenant les données des clients
    excel_file_path = "C:\\Users\\Audric\\Projets\\MailAutomatisation\\emails.xlsx"

    # Chargement des données depuis le fichier Excel
    data = load_data_from_excel(excel_file_path)

    # Dossier contenant les fichiers PDF à envoyer
    pdf_folder_path = "C:\\Users\\Audric\\Projets\\MailAutomatisation\\pdf_test"

    # Parcourir les fichiers PDF dans le dossier et envoyer un e-mail pour chaque fichier
    for filename in os.listdir(pdf_folder_path):
        if not filename.endswith(".pdf"):
            continue

        # Récupération du nom et du prénom du destinataire à partir du nom du fichier PDF
        nom_prenom = extract_nom_prenom_from_filename(filename, data)

        # Si aucun adhérent n'a été trouvé dans le nom du fichier, passer au fichier suivant
        if nom_prenom is None:
            print(f"Aucune correspondance trouvée dans le nom du fichier {filename}")
            continue

        nom, prenom = nom_prenom
        
        # Afficher le nom du fichier PDF associé à la personne
        print(f"Le fichier {filename} est associé à {nom} {prenom}")

        # Récupération de l'adresse e-mail du destinataire à partir du nom et du prénom
        """
        dest_info = data.get((nom, prenom))
        if dest_info is None:
            print(f"Aucune information trouvée pour {nom} {prenom}")
            continue
        else:
            print(f"Information trouvée pour {nom} {prenom}")

        dest_email = dest_info.get('email')
        if dest_email is None:
            print(f"Aucune adresse e-mail trouvée pour {nom} {prenom}")
            continue

        # Chemin du fichier PDF à joindre
        pdf_file_path = os.path.join(pdf_folder_path, filename)

        # Envoyer l'e-mail
        # send_email_with_attachment(nom, prenom, dest_email, pdf_file_path)
        """
if __name__ == '__main__':
    main()