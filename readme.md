# automatisation d'envoie des places de match

## Prérequis :
- Un PC sous Windows avec accès à Internet.
- Python 3.x n'est pas nécessairement installé sur le PC, car nous allons utiliser une distribution Python portable.

## Installation de Python :
1. Téléchargez la dernière version de la distribution portable Python (pour Windows) à partir de https://www.python.org/downloads/windows/.
2. Exécutez le programme d'installation et suivez les instructions à l'écran pour installer Python. Assurez-vous de cocher la case "Ajouter Python xx.x au PATH" lors de l'installation.

## Configuration de l'environnement :
3. Créez un nouveau dossier pour votre projet sur votre PC.
4. Placez le fichier envoyer_emails.py et le fichier Excel contenant les adresses e-mail des destinataires dans ce dossier.
5. Ouvrez une invite de commandes (CMD) et naviguez jusqu'au dossier de votre projet à l'aide de la commande cd.
6. Tapez la commande suivante pour créer un environnement virtuel pour votre projet : python -m venv env.
7. Activez l'environnement virtuel en exécutant la commande suivante : .\env\Scripts\activate (Notez que le chemin vers le script activate peut varier selon la version de Windows que vous utilisez).
8. Installez les dépendances nécessaires pour ce projet en exécutant la commande suivante : pip install openpyxl.

## Exécution du script :
9. Assurez-vous que l'environnement virtuel est activé en exécutant la commande .\env\Scripts\activate.
10. Exécutez le script en tapant la commande suivante : python envoyer_emails.py.
11. Le script vous demandera d'entrer l'adresse e-mail et le mot de passe associé au compte Gmail que vous souhaitez utiliser pour envoyer les e-mails. Entrez les informations demandées.
12. Le script enverra les e-mails à tous les destinataires répertoriés dans le fichier Excel. Veuillez noter que le nom de fichier et le chemin d'accès dans le code doivent correspondre au nom et au chemin d'accès réels du fichier Excel sur votre ordinateur.

Notes :
- Si vous rencontrez des erreurs lors de l'installation ou de l'exécution du script, veuillez contacter votre administrateur système ou votre développeur pour obtenir de l'aide.
- Assurez-vous de ne pas inclure votre mot de passe Gmail dans le script lui-même ou dans le fichier Excel contenant les adresses e-mail des destinataires.
- Il est recommandé de tester le script avec un petit nombre de destinataires avant de l'utiliser pour envoyer des e-mails à grande échelle.