import unittest
import inspect
import re

# Présumons que nous avons déjà cette liste en mémoire
# (Dans une étape ultérieure, nous pouvons ajouter du code pour lire les données Excel)
adhérents = [
    ("Daviau", "Audric"),
    ("Daviau", "Audric"),
    ("Hautin", "Alexandra"),
    ("Truc-Machin", "Thomas"),
    ("Martin", "Jean-Pierre"),
    ("Geigenholz", "Corentin"),
]

def extract_nom_prenom_from_filename(filename):
    # Retire l'extension du nom du fichier
    filename = filename.split('.')[0]

    # Vérifie chaque adhérent pour voir s'il est dans le nom du fichier
    for nom, prenom in adhérents:
        nom_lower = nom.lower()
        prenom_lower = prenom.lower()

        # Si le prénom est dans le nom du fichier, recherche le nom et le prénom
        if prenom_lower in filename.lower():
            pattern = f"{prenom_lower}.*{nom_lower}|{nom_lower}.*{prenom_lower}"
            if re.search(pattern, filename.lower()):
                return (nom, prenom)
        # Si le prénom n'est pas dans le nom du fichier, recherche seulement le nom
        else:
            if nom_lower in filename.lower():
                return (nom, prenom)

    # Si aucun adhérent n'a été trouvé, retourne None
    return None


class TestExtractNomPrenomFromFilename(unittest.TestCase):
    def test_extract_nom_prenom_from_filename(self):
        test_cases = [
            ("4-Daviau-Audric.pdf", ("Daviau", "Audric")),
            ("4_Daviau_Audric.pdf", ("Daviau", "Audric")),
            ("4 Daviau Audric.pdf", ("Daviau", "Audric")),
            ("Audric_Daviau.pdf", ("Daviau", "Audric")),
            ("4-DAVIAU-AUDRIC.pdf", ("Daviau", "Audric")),
            ("4-daviau-audric.pdf", ("Daviau", "Audric")),
            ("4-Daviau-Lemaitre-Audric.pdf", ("Daviau", "Audric")),
            ("OM GEIGENHOLZ CORENTIN.pdf", ("Geigenholz", "Corentin")),
            ("LENS GEIGENHOLZ CORENTIN.pdf", ("Geigenholz", "Corentin")),
            ("LYON_GEIGENHOLZ CORENTIN.pdf", ("Geigenholz", "Corentin")),
            ("7_BE_GEIGENHOLZ_52402100255.pdf", ("Geigenholz", "Corentin")),
            ("4_BE_GEIGENHOLZCORENTIN_53002100822.pdf", ("Geigenholz", "Corentin")),
        ]

        for idx, (filename, expected_result) in enumerate(test_cases):
            with self.subTest(filename=filename):
                result = extract_nom_prenom_from_filename(filename)
                self.assertEqual(result, expected_result, f"Fail line {inspect.currentframe().f_lineno} (test case {idx + 1})")

if __name__ == '__main__':
    unittest.main()